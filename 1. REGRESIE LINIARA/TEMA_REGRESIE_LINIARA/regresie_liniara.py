import numpy as np
import matplotlib.pyplot as plt


def get_data():
    file = open('salary.csv')

    data_set = np.genfromtxt('salary.csv', dtype=float, delimiter=",", names=['YearsExperience', 'Salary'],
                             unpack=True, skip_header=True)
    x = np.array(data_set['YearsExperience'])
    y = np.array(data_set['Salary'])

    file.close()

    return x, y


def data_plot(x, y):
    # Make first plot without linear regression.
    plt.figure("INCOME FRAME", figsize=(13, 8), dpi=100)
    plt.scatter(x[0:30], y[0:30], label="train data",
                color="#35d3da", marker="o", s=100)
    plt.xlabel('Years of Experience')
    plt.ylabel('Salary')
    plt.title('Plot without Linear Regression')
    plt.legend(loc='best', fontsize="medium")
    axes = plt.gca()
    axes.set_xlim([0, 20])
    axes.set_ylim([30000, 220000])
    plt.show()


def array_to_2d_array(array):
    # The shape tool gives a tuple of array dimensions and can be used to change the dimensions of an array.
    # The reshape tool gives a new shape to an array without changing its data.
    array = array.reshape(array.shape[0], -1)

    return array


def build_bias(array):
    # Return an array of ones with the same shape and type as a given array.
    return np.ones_like(array)


def h_function(x, w):
    return w[0] * x + w[1]


def normal_equation(x, y):
    x_matrix = array_to_2d_array(x)
    y_matrix = array_to_2d_array(y)

    bias = build_bias(x_matrix)
    x_bias = np.append(x_matrix, bias, axis=1)

    # Moore-Penrose pseudo-inverse of a matrix.
    x_pseudo_inverse = np.linalg.pinv(x_bias)

    return np.matmul(x_pseudo_inverse, y_matrix)


def linear_regression_plot(x, y, weight):
    # Make first plot without linear regression.
    plt.figure("INCOME FRAME", figsize=(13, 8), dpi=100)
    plt.scatter(x[0:29], y[0:29], label="train data",
                color="#35d3da", marker="o", s=100)
    plt.scatter(x[30:], y[30:], label="test data",
                color="#fe1100", marker="o", s=100)
    y_prediction = h_function(x, weight)
    plt.plot(x, y_prediction, color="#e3da24", ls="-", label='regression line')
    plt.xlabel('Years of Experience')
    plt.ylabel('Salary')
    plt.title('Plot with Linear Regression')
    plt.legend(loc='best', fontsize="medium")
    axes = plt.gca()
    axes.set_xlim([0, 20])
    axes.set_ylim([30000, 220000])
    plt.show()


def main():
    x_years_of_exp, y_salaries = get_data()

    weight = normal_equation(x_years_of_exp, y_salaries)

    data_plot(x_years_of_exp, y_salaries)

    linear_regression_plot(x_years_of_exp, y_salaries, weight)


if __name__ == '__main__':
    main()
